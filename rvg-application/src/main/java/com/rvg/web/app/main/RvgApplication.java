package com.rvg.web.app.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Import({})
@SpringBootApplication
@EnableConfigurationProperties
public class RvgApplication {
	
	private final Logger log=LoggerFactory.getLogger(RvgApplication.class);
	
	
	public static void main(String[] args) {
		
		SpringApplication.run(RvgApplication.class, args);
	}

}
